/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package builder;
import java.util.*;

/**
 *
 * @author Alma Rosa Rodriguez Santos
 */
public class RobotHamburguesa implements Robot{
    List<Integer> acciones;
    
    public RobotHamburguesa(){}
    
    private void iniciar(){
        System.out.println("Iniciando la hamburguesa");
    }
    
    private void getIngredientes(){
        System.out.println("Buscando: pan, hamburguesa, salsas");
    }
    
    private void armar(){
        System.out.println("Armando la hamburguesa");
    }
    
    private void revisar(){
        System.out.println("Revisando proceso");
    }
    
    private void terminar(){
        System.out.println("Terminando proceso");
    }
    
    @Override
    public void cargaAcciones(List<Integer> accion){
    this.acciones = accion;
    }
    
    @Override
    public void trabajar(){
    iniciar();
    for (Integer i: acciones){
    switch(i){
    case 1:
        getIngredientes();
        break;
    case 2:
        armar();
        break;
    case 3:
        revisar();
        break;
    default:
        System.out.println("Esa accion no la puedo hacer");
    }
    }
    terminar();
    }    
}
