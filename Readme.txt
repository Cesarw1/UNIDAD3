CESAR CORDOVA CALDERON ITI 9-3

Patrones de dise�o.

Singleton
Es un patr�n dise�ado para limitar la creaci�n de objetos pertenecientes a una clase. El objetivo de este patr�n es el de garantizar que una clase solo tenga una instancia (o ejemplar) y proporcionar un punto de acceso global a ella. Esta patr�n; por ejemplo, suele ser utilizado para las conexiones a bases de datos.

Factory
Uno de los patrones de dise�o m�s utilizados en Java es el patr�n Factory que es un patr�n de dise�o creacional y que sirve para construir una jerarqu�a de clases. Podemos utilizar este patr�n cuando definamos una clase a partir de la que se crear�n objetos pero sin saber de qu� tipo son, siendo otras subclases las encargadas de decidirlo.

Builder
El objetivo del patr�n Builder es abstraer la construcci�n de objetos complejos de su implementaci�n, de modo que un cliente pueda crear objetos complejos sin tener que preocuparse de las diferencias en su implantaci�n. Este patr�n puede ser utilizado cuando necesitemos crear objetos complejos compuestos de varias partes independientes.

Prototipo
El objetivo es crear a partir de un modelo.Permite crear objetos predise�ados sin conocer detalles de c�mo crearlos. Esto lo logra especificando los prototipos de objetos a crear. Los nuevos objetos que se crear�n de los prototipos, en realidad, son clonados. Este patr�n nos ser� �til si necesitamos crear y manipular copias de otros objetos.

Proxy
El patr�n Proxy se utiliza como intermediario para acceder a un objeto, permitiendo controlar el acceso a �l. Para ello obliga que las llamadas a un objeto ocurran indirectamente a trav�s de un objeto proxy, que act�a como un sustituto del objeto original, delegando luego las llamadas a los m�todos de los objetos respectivos. Este patr�n se basa en proporcionar un objeto que haga de intermediario (proxy) de otro, para controlar el acceso a �l.

Patron de dise�o que se aplicara: 

El Patr�n Singleton tambi�n se conoce como Instancia �nica, su objetivo es restringir la creaci�n de objetos  pertenecientes a una clase, de modo que solo se tenga una �nica instancia de la clase para toda la aplicaci�n, garantizando as� un punto de acceso global al objeto creado. 

Para implementarlo, la clase Singleton debe tener un constructor privado que solo sera accedido desde la misma clase, se crea tambi�n una instancia privada de la clase, as� como un m�todo est�tico que permita el acceso a dicha instancia de la forma ClaseSingleton.getInstanciaSingleton();


Pruebas unitarias con JUnit.

Vamos a utilizar JUnit para probar los m�todos. Para ello deberemos crear una serie de clases en las que implementaremos las pruebas dise�adas. Esta implementaci�n consistir� b�sicamente en invocar el m�todo que est� siendo probado pas�ndole los par�metros de entrada establecidos para cada caso de prueba, y comprobar si la salida real coincide con la salida esperada. Esto en principio lo podr�amos hacer sin necesidad de utilizar JUnit, pero el utilizar esta herramienta nos va a ser de gran utilidad ya que nos proporciona un framework que nos obligar� a implementar las pruebas en un formato est�ndar que podr� ser reutilizable y entendible por cualquiera que conozca la librer�a.